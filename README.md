# mPaaS
:round_pushpin: 注： 系统处于开发阶段, 预计**两个月**时间完成整体架构体系

[中文说明](https://gitee.com/ibyte/M-Pass/blob/master/README.md)  | 
[英文说明](https://gitee.com/ibyte/M-Pass/blob/master/README.en.md)

#### 介绍
开发平台 mPaaS（Microservice PaaS）为 租户级别 开发、测试、运营及运维提供云到端的一站式解决方案，能有效降低技术门槛、减少研发成本、提升开发效率，协助企业快速搭建稳定高质量的Pass平台应用

## 核心功能
#### 1.快速开发
工程化的开发框架可以自动生成初始化代码，框架还提供模块化开发模式，适用于多人协作开发。
#### 2.性能优化
所有组件都经历了高并发，大流量的检验，对弱网，保活，容器等都有深度的优化，能够兼容复杂的客户端情况
#### 3.数字化运营闭环
支持运营活动投放一站式全流程创建管理，加载智能化投放能力，最大可能提升运营效率和转化效果，助力业务增长。
#### 4.使用方式灵活
框架与组件并没有强依赖，可分可合，灵活机动。各组件可以独立的提供强大的功能，也可以互相配合优化使用体验，发挥更大的作用

#### 社区讨论
<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=916c90a224d93e4eb12d32f1973906cf8bb1c7a45edb62010b137f694fcdb7cc"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="M-Pass" title="M-Pass"></a>

### 核心JAR依赖视图
![输入图片说明](https://images.gitee.com/uploads/images/2019/1009/000659_b0861629_1468963.png "JAR.png")

## 应用场景
####  一 App 性能提速
![App 性能提速](https://images.gitee.com/uploads/images/2019/1008/192247_1c6d70a1_1468963.jpeg "App性能提升.jpg")
####  二 研发效能提升
![研发效能提升](https://images.gitee.com/uploads/images/2019/1008/192259_f2fefd0d_1468963.jpeg "研发效能提升.jpg")
####  三 数字化运营
![数字化运营](https://images.gitee.com/uploads/images/2019/1008/192445_511743b1_1468963.jpeg "数字化运营.jpg")
