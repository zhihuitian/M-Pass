# mPaaS
> Note: The system is in the development stage and it is expected to complete the overall architecture system in two months.

#### Introduction
The development platform mPaaS (Microservice PaaS) provides a cloud-to-end one-stop solution for tenant-level development, testing, operation and operation and maintenance. It can effectively reduce technical thresholds, reduce R&D costs, improve development efficiency, and help enterprises quickly build stable and high-quality products. Pass platform application

## Core functions
#### 1.Rapid development
The engineering development framework automatically generates initialization code, and the framework also provides a modular development model for multi-person collaborative development
#### 2.Performance optimization
All components have experienced high concurrency, high traffic inspection, deep optimization for weak networks, keep-alives, containers, etc., and are compatible with complex client situations
#### 3.Digital operation closed loop
Support operational activities to deliver one-stop full-process creation management, load intelligent delivery capabilities, and maximize operational efficiency and conversion results to help business growth.
#### 4.Flexible use
The framework and components are not strongly dependent, they are separable and flexible. Each component can provide powerful functions independently, and can also work together to optimize the user experience and play a greater role.

### JAR Rely
![输入图片说明](https://images.gitee.com/uploads/images/2019/1009/000821_c4d97493_1468963.png "JAR.png")

